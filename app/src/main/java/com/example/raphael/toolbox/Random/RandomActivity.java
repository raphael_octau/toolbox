package com.example.raphael.toolbox.Random;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;

import com.example.raphael.toolbox.R;

public class RandomActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_random);
        setTitle(R.string.random_title);
        if (getSupportActionBar() != null) getSupportActionBar().setElevation(0);

        final NumberPicker numberPicker1 = (NumberPicker) findViewById(R.id.random_numberpicker1);
        final NumberPicker numberPicker2 = (NumberPicker) findViewById(R.id.random_numberpicker2);

        numberPicker1.setMinValue(0);
        numberPicker1.setMaxValue(Integer.MAX_VALUE - 2);
        numberPicker1.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
                if (newVal >= numberPicker2.getValue())
                    numberPicker2.setValue(newVal + 1);
            }
        });

        numberPicker2.setMinValue(1);
        numberPicker2.setMaxValue(Integer.MAX_VALUE - 1);

        final TextView txtRes = (TextView) findViewById(R.id.random_text);
        txtRes.setTextIsSelectable(true);

        findViewById(R.id.random_generer).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                txtRes.setText(String.valueOf(numberPicker1.getValue() + (int) (Math.random() * ((numberPicker2.getValue() - numberPicker1.getValue()) + 1))));
            }
        });
    }
}
