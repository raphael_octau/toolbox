package com.example.raphael.toolbox;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.AttrRes;
import android.support.annotation.ColorInt;
import android.util.TypedValue;

public class Outils {

    @ColorInt
    public static int getColorID(Context context, @AttrRes int idColor) {
        TypedValue typedValue = new TypedValue();

        TypedArray a = context.obtainStyledAttributes(typedValue.data, new int[]{idColor});
        int color = a.getColor(0, 0);

        a.recycle();

        return color;
    }

    static final float ALPHA = 0.05f;

    public static float[] lowPass(float[] input, float[] output) {
        if (output == null) return input;

        for (int i = 0; i < input.length; i++) {
            output[i] = output[i] + ALPHA * (input[i] - output[i]);
        }
        return output;
    }

}
