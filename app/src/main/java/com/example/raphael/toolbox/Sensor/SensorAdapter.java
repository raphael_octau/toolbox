package com.example.raphael.toolbox.Sensor;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.hardware.Sensor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.raphael.toolbox.R;

import java.util.LinkedHashMap;

class SensorAdapter extends BaseAdapter {

    private Activity activity;
    private LinkedHashMap<Sensor, String[]> data;
    private static LayoutInflater inflater = null;

    SensorAdapter(Activity a, LinkedHashMap<Sensor, String[]> d) {
        activity = a;
        data = d;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public int getCount() {
        return data.size();
    }

    public Object getItem(int position) {
        return position;
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(final int position, View convertView, ViewGroup parent) {
        View vi = convertView;
        if (convertView == null)
            vi = inflater.inflate(R.layout.sensor_row, null);

        Sensor sensorActuel = null;
        int i = 0;
        for (Sensor sensor : data.keySet())
            if (i++ == position) sensorActuel = sensor;

        final Sensor finalSensorActuel = sensorActuel;
        vi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v7.app.AlertDialog alertDialog = new android.support.v7.app.AlertDialog.Builder(activity).create();
                alertDialog.setTitle(activity.getResources().getString(R.string.activity_sensor_dialog_titre, data.get(finalSensorActuel)[0]));
                if (finalSensorActuel != null)
                    alertDialog.setMessage(activity.getResources().getString(R.string.activity_sensor_dialog_nom, finalSensorActuel.getName()) + "\n" +
                            activity.getResources().getString(R.string.activity_sensor_dialog_vendeur, finalSensorActuel.getVendor()) + "\n" +
                            activity.getResources().getString(R.string.activity_sensor_dialog_version, String.valueOf(finalSensorActuel.getVersion())) + "\n" +
                            activity.getResources().getString(R.string.activity_sensor_dialog_maxrange, String.valueOf(finalSensorActuel.getMaximumRange())) + "\n" +
                            activity.getResources().getString(R.string.activity_sensor_dialog_resolution, String.valueOf(finalSensorActuel.getResolution())) + "\n" +
                            activity.getResources().getString(R.string.activity_sensor_dialog_puissance, String.valueOf(finalSensorActuel.getPower())) + "\n" +
                            activity.getResources().getString(R.string.activity_sensor_dialog_mindelai, String.valueOf(finalSensorActuel.getMinDelay())));
                else
                    alertDialog.setMessage(activity.getResources().getString(R.string.activity_sensor_dialog_aucun));
                alertDialog.setButton(android.support.v7.app.AlertDialog.BUTTON_POSITIVE, activity.getResources().getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
            }
        });

        TextView titre = (TextView) vi.findViewById(R.id.sensor_title);
        TextView info = (TextView) vi.findViewById(R.id.sensor_info);

        titre.setText(data.get(sensorActuel)[0]);
        if (sensorActuel != null) {
            info.setText(data.get(sensorActuel)[1]);
            titre.setTextColor(Color.parseColor("#000000")); //Couleur de base selon Google
            titre.setAlpha(0.54f);
            info.setVisibility(View.VISIBLE);
        } else {
            titre.setTextColor(Color.rgb(255, 100, 100));
            titre.setAlpha(1f);
            info.setVisibility(View.GONE);
        }
        return vi;
    }
}