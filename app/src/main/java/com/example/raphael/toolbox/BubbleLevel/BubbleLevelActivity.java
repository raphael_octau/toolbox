package com.example.raphael.toolbox.BubbleLevel;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;

import com.example.raphael.toolbox.Outils;
import com.example.raphael.toolbox.R;

import java.util.ArrayList;

public class BubbleLevelActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private DrawView drawView;
    private boolean modeFlat;

    private ArrayList<float[]> alValues;
    private final int NB_AVERAGE = 10;

    private int colorPrimary, colorAccent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.bubblelevel_title);

        colorPrimary = Outils.getColorID(this, R.attr.colorPrimary);
        colorAccent = Outils.getColorID(this, R.attr.colorAccent);

        alValues = new ArrayList<>();
        drawView = new DrawView(this);
        modeFlat = true;
        //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT); //On empeche la rotation en mode flat

        setContentView(drawView);

        //On s'enregistre comme listener de l'accelerometre
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
    }

    /**
     * On créé le menu de changement de mode dynamiquement
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        if (modeFlat)
            menu.add(0, 0, Menu.NONE, R.string.bubblelevel_menu_vertical).setIcon(R.drawable.bubblelevel_vertical).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        else
            menu.add(0, 0, Menu.NONE, R.string.bubblelevel_menu_flat).setIcon(R.drawable.bubblelevel_flat).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case 0:
                if (modeFlat) {
                    item.setIcon(R.drawable.bubblelevel_flat);
                    item.setTitle(R.string.bubblelevel_menu_flat);
                    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
                } else {
                    item.setIcon(R.drawable.bubblelevel_vertical);
                    item.setTitle(R.string.bubblelevel_menu_vertical);
                    //setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                }
                drawView.invalidate();
                modeFlat = !modeFlat;
                break;
        }
        return false;
    }

    /**
     * On enregistre le mode actuel pour le retrouver lors d'une rotation de l'écran par exemple
     */
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean("mode", modeFlat);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        modeFlat = savedInstanceState.getBoolean("mode");
    }

    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
    }

    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    /**
     * On écoute l'accelerometre
     */
    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            alValues.add(event.values);
            if (alValues.size() == NB_AVERAGE) {
                float[] tabValues = new float[3];
                for (int i = 0; i < alValues.size(); i++) {
                    tabValues[0] += alValues.get(i)[0];
                    tabValues[1] += alValues.get(i)[1];
                    tabValues[2] += alValues.get(i)[2];
                }
                tabValues[0] /= NB_AVERAGE;
                tabValues[1] /= NB_AVERAGE;
                tabValues[2] /= NB_AVERAGE;
                alValues.clear();
                drawView.setValues(tabValues);
                drawView.invalidate();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @SuppressWarnings("deprecation")
    private int getOrientation() {
        return ((WindowManager) getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
    }

    /**
     * Classe qui permet de peindre sur l'écran
     */
    class DrawView extends View {

        private Paint paint = new Paint();
        private float[] values;
        private float posX, posY, angle;

        public DrawView(Context context) {
            super(context);
            values = new float[]{0, 0, 0};
            paint.setColor(Color.BLACK);
            setBackgroundColor(Color.WHITE);
            paint.setAntiAlias(true);
        }

        public void setValues(float[] values) {
            //On prend la norme du vecteur
            double norm_Of_g = Math.sqrt(values[0] * values[0] + values[1] * values[1] + values[2] * values[2]);

            // On normalise le vecteur et on fais la moyenne avec le prevedent
            values[0] = (float) ((this.values[0] + (values[0] / norm_Of_g)) / 2);
            values[1] = (float) ((this.values[1] + (values[1] / norm_Of_g)) / 2);
            values[2] = (float) ((this.values[2] + (values[2] / norm_Of_g)) / 2);

            this.values = values;
        }

        @Override
        public void onDraw(Canvas canvas) {
            paint.setColor(colorPrimary);
            //Mode a plat ou vertical
            if (modeFlat) drawFlat(canvas);
            else drawVertical(canvas);
        }

        /**
         * On dessine le mode a plat
         *
         * @param canvas le canvas
         */
        private void drawFlat(Canvas canvas) {
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(10f);
            //On recupere la largeure de la cible par rapport a la taille de l'écran
            float largeurCible = (getWidth() > getHeight()) ? getHeight() / 2 : getWidth() / 2;

            //On dessine la cible
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, largeurCible - (largeurCible / 5), paint);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, largeurCible / 2, paint);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, (largeurCible / 5), paint);
            canvas.drawLine(getWidth() / 2, getHeight() / 2 - (largeurCible) + (largeurCible / 5), getWidth() / 2, getHeight() / 2 + (largeurCible) - (largeurCible / 5), paint);
            canvas.drawLine(getWidth() / 2 - (largeurCible) + (largeurCible / 5), getHeight() / 2, getWidth() / 2 + (largeurCible) - (largeurCible / 5), getHeight() / 2, paint);

            paint.setStyle(Paint.Style.FILL);

            //On recupère les coordonnees du point
            switch (getOrientation()) {
                case Surface.ROTATION_90: //Paysage
                    posX = -values[1] * (largeurCible - (largeurCible / 5));
                    posY = -values[0] * (largeurCible - (largeurCible / 5));
                    break;
                case Surface.ROTATION_180: //Portrait retourné
                    posX = -values[0] * (largeurCible - (largeurCible / 5));
                    posY = values[1] * (largeurCible - (largeurCible / 5));
                    break;
                case Surface.ROTATION_270: //Paysage retourné
                    posX = values[1] * (largeurCible - (largeurCible / 5));
                    posY = values[0] * (largeurCible - (largeurCible / 5));
                    break;
                case Surface.ROTATION_0: //Portrait
                default:
                    posX = values[0] * (largeurCible - (largeurCible / 5));
                    posY = -values[1] * (largeurCible - (largeurCible / 5));
                    break;
            }

            //On met le point en vert si il est "a peu près" centré
            if (posX < 10 && posX > -10 && posY < 10 && posY > -10)
                paint.setColor(colorAccent);

            //On dessine le point
            canvas.drawCircle(posX + getWidth() / 2, posY + getHeight() / 2, (largeurCible / 5.5f), paint);

            //On ecrit les coordonnees
            paint.setColor(colorPrimary);
            paint.setTextSize(largeurCible / 10);
            canvas.drawText(Math.round(posX * 90 / (largeurCible - (largeurCible / 5))) + "°", getWidth() / 2 - largeurCible + (largeurCible / 5) + 10, getHeight() / 2 - 10, paint);
            canvas.drawText(-Math.round(posY * 90 / (largeurCible - (largeurCible / 5))) + "°", getWidth() / 2 + 10, getHeight() / 2 - largeurCible + (largeurCible / 5) - 10, paint);
        }

        /**
         * On dessine le mode vertical
         *
         * @param canvas le canvas
         */
        private void drawVertical(Canvas canvas) {
            canvas.save(); //On mémorise la config actuelle pour la rétablir apres la rotation

            //On recupère l'angle
            switch (getOrientation()) {
                case Surface.ROTATION_90: //Paysage
                    angle = (int) Math.toDegrees(Math.atan2(values[0], values[1])) - 90;
                    break;
                case Surface.ROTATION_180: //Portrait retourné
                    angle = (int) Math.toDegrees(Math.atan2(values[0], values[1])) - 180;
                    break;
                case Surface.ROTATION_270: //Paysage retourné
                    angle = (int) Math.toDegrees(Math.atan2(values[0], values[1])) + 90;
                    break;
                case Surface.ROTATION_0: //Portrait
                default:
                    angle = (int) Math.toDegrees(Math.atan2(values[0], values[1]));
                    break;
            }

            //On met en vert si l'angle est "à peu près" 0
            if (angle < 2 && angle > -2)
                paint.setColor(colorAccent);

            //On dessine l'inclinaison
            if (angle > -75 && angle < 75) {
                canvas.rotate(angle, getWidth() / 2, getHeight() / 2);
                canvas.drawRect(-getWidth(), getHeight() / 2, getWidth() * 2, getHeight() * 2, paint);
            } else {
                canvas.drawRect(0, 0, getWidth(), getHeight(), paint);
            }

            canvas.restore(); //On rétablit la config précédente

            //On ecrit la valeur de l'angle
            paint.setColor(Color.WHITE);
            String texte = Math.round(-angle) + "°";
            paint.setTextSize((getWidth() > getHeight()) ? getHeight() / 4 : getWidth() / 4);
            //On centre le texte
            Rect r = new Rect();
            canvas.getClipBounds(r);
            int cHeight = r.height() / 2 + r.height();
            int cWidth = r.width();
            paint.setTextAlign(Paint.Align.LEFT);
            paint.getTextBounds(texte, 0, texte.length(), r);
            float x = cWidth / 2f - r.width() / 2f - r.left;
            float y = cHeight / 2f + r.height() / 2f - r.bottom;
            //On l'ecrit
            canvas.drawText(texte, x, y, paint);
        }

    }
}