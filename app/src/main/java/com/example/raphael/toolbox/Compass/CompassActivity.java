package com.example.raphael.toolbox.Compass;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.example.raphael.toolbox.Outils;
import com.example.raphael.toolbox.R;

public class CompassActivity extends AppCompatActivity implements SensorEventListener {
    private SensorManager sensorManager;
    private Sensor accelerometre;
    private Sensor magnetometre;

    private ViewBoussole viewBoussole;

    private int colorPrimary, colorAccent;
    private String nord, sud, est, ouest;

    private Float angle;
    private float[] valeursAccelerometre;
    private float[] valeursMagnetometre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.compass_title);

        colorPrimary = Outils.getColorID(this, R.attr.colorPrimary);
        colorAccent = Outils.getColorID(this, R.attr.colorAccent);
        nord = getResources().getString(R.string.compass_nord);
        sud = getResources().getString(R.string.compass_sud);
        est = getResources().getString(R.string.compass_est);
        ouest = getResources().getString(R.string.compass_ouest);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_NORMAL);

        viewBoussole = new ViewBoussole(this);
        viewBoussole.setBackgroundColor(Color.WHITE);

        setContentView(viewBoussole);

        accelerometre = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        magnetometre = sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            valeursAccelerometre = Outils.lowPass(event.values.clone(), valeursAccelerometre);

        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            valeursMagnetometre = Outils.lowPass(event.values.clone(), valeursMagnetometre);

        if (valeursAccelerometre != null && valeursMagnetometre != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            if (SensorManager.getRotationMatrix(R, I, valeursAccelerometre, valeursMagnetometre)) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                if (angle != null) {
                    if (Math.abs(angle - orientation[0]) > 0.02) { //Pour éviter les intermitences de 1 degré
                        angle = orientation[0];
                        viewBoussole.invalidate();
                    }
                } else {
                    angle = orientation[0];
                    viewBoussole.invalidate();
                }
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {
    }

    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometre, SensorManager.SENSOR_DELAY_FASTEST);
        sensorManager.registerListener(this, magnetometre, SensorManager.SENSOR_DELAY_FASTEST);
    }

    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    private String getOrientationLetters(float angle) {
        if (angle > -157.5 && angle <= -112.5) return sud + ouest;
        else if (angle > -112.5 && angle <= -67.5) return ouest;
        else if (angle > -67.5 && angle <= -22.5f) return nord + ouest;
        else if (angle > -22.5f && angle <= 22.5f) return nord;
        else if (angle > 22.5f && angle <= 67.5) return nord + est;
        else if (angle > 67.5 && angle <= 112.5) return est;
        else if (angle > 112.5 && angle <= 157.5) return sud + est;
        else return sud;
    }

    public class ViewBoussole extends View {
        private int centreX;
        private int centreY;
        float coordX, coordY;

        private Rect zoneTexte = new Rect();
        private Path path = new Path();
        private Point p1, p2, p3;

        private Paint paint = new Paint();

        public ViewBoussole(Context context) {
            super(context);
            paint.setAntiAlias(true);
        }

        @Override
        public void onDraw(Canvas canvas) {
            if(angle == null) return;

            centreX = getWidth() / 2;
            centreY = getHeight() / 2;

            canvas.drawColor(Color.rgb(35, 35, 35));

            paint.setStyle(Paint.Style.FILL);

            if ((float) (-angle * 360 / (2 * Math.PI)) > -10 && (float) (-angle * 360 / (2 * Math.PI)) < 10)
                paint.setColor(colorAccent);
            else
                paint.setColor(colorPrimary);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, getWidth() / 2.5f, paint);
            paint.setColor(Color.rgb(50, 50, 50));
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, getWidth() / 2.7f, paint);
            paint.setColor(Color.rgb(35, 35, 35));
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, getWidth() / 4.5f, paint);
            paint.setColor(colorPrimary);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2, getWidth() / 6, paint);

            p1 = new Point(getWidth() / 2, (int) (getHeight() / 2 - getWidth() / 4.5f));
            coordX = (float) ((getWidth() / 6) * Math.cos(Math.toRadians(220)) + (getWidth() / 2));
            coordY = (float) ((getWidth() / 6) * Math.sin(Math.toRadians(220)) + (getHeight() / 2));
            p2 = new Point((int) coordX + 1, (int) coordY);
            coordX = (float) ((getWidth() / 6) * Math.cos(Math.toRadians(-40)) + (getWidth() / 2));
            coordY = (float) ((getWidth() / 6) * Math.sin(Math.toRadians(-40)) + (getHeight() / 2));
            p3 = new Point((int) coordX - 1, (int) coordY);
            path.reset();
            path.moveTo(p1.x, p1.y);
            path.lineTo(p2.x, p2.y);
            path.lineTo(p3.x, p3.y);
            path.lineTo(p1.x, p1.y);
            path.lineTo(p1.x, p1.y);
            path.close();
            canvas.drawPath(path, paint);


            //Affichage du texte au milieu
            paint.setTextSize(getWidth() / 10);
            paint.setColor(Color.WHITE);
            String texte = Integer.toString((int) ((Math.toDegrees(angle) + 360) % 360)) + "°";
            //On centre le 1er texte
            canvas.getClipBounds(zoneTexte);
            int cHeight = zoneTexte.height();
            int cWidth = zoneTexte.width();
            paint.setTextAlign(Paint.Align.LEFT);
            paint.getTextBounds(texte, 0, texte.length(), zoneTexte);
            float x = cWidth / 2f - zoneTexte.width() / 2f - zoneTexte.left;
            float y = cHeight / 2f + zoneTexte.height() / 2f - zoneTexte.bottom - ((paint.descent() - paint.ascent()) / 2);
            //On l'ecrit
            canvas.drawText(texte, x, y, paint);

            texte = getOrientationLetters((float) (angle * 360 / (2 * Math.PI)));
            //On centre le 2eme texte
            canvas.getClipBounds(zoneTexte);
            cHeight = zoneTexte.height();
            cWidth = zoneTexte.width();
            paint.setTextAlign(Paint.Align.LEFT);
            paint.getTextBounds(texte, 0, texte.length(), zoneTexte);
            x = cWidth / 2f - zoneTexte.width() / 2f - zoneTexte.left;
            y = cHeight / 2f + zoneTexte.height() / 2f - zoneTexte.bottom + ((paint.descent() - paint.ascent()) / 2);
            //On l'ecrit
            canvas.drawText(texte, x, y, paint);

            //Tourne en fonction de l'angle calculé dans onSensorChanged()
            if (angle != null)
                canvas.rotate((float) (-angle * 360 / (2 * Math.PI)), centreX, centreY);


            //Affichage de la pointe
            if ((float) (-angle * 360 / (2 * Math.PI)) > -10 && (float) (-angle * 360 / (2 * Math.PI)) < 10)
                paint.setColor(colorAccent);
            else
                paint.setColor(colorPrimary);
            p1 = new Point(getWidth() / 2, (int) (getHeight() / 2 - getWidth() / 3f));
            coordX = (float) ((getWidth() / 2.5f) * Math.cos(Math.toRadians(-100)) + (getWidth() / 2));
            coordY = (float) ((getWidth() / 2.5f) * Math.sin(Math.toRadians(-100)) + (getHeight() / 2));
            p2 = new Point((int) coordX, (int) coordY);
            coordX = (float) ((getWidth() / 2.5f) * Math.cos(Math.toRadians(-80)) + (getWidth() / 2));
            coordY = (float) ((getWidth() / 2.5f) * Math.sin(Math.toRadians(-80)) + (getHeight() / 2));
            p3 = new Point((int) coordX, (int) coordY);
            path.reset();
            path.moveTo(p1.x, p1.y);
            path.lineTo(p2.x, p2.y);
            path.lineTo(p3.x, p3.y);
            path.lineTo(p1.x, p1.y);
            path.lineTo(p1.x, p1.y);
            path.close();
            canvas.drawPath(path, paint);

            //On dessine les points cardinaux
            paint.setColor(Color.WHITE);
            paint.setTextSize((getWidth() / 2.7f - getWidth() / 4.5f) / 2);
            canvas.drawText(nord, getWidth() / 2 - paint.measureText(nord) / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 4, paint);

            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 2, getWidth() / 100, paint);
            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 2, getWidth() / 100, paint);
            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 2, getWidth() / 100, paint);

            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawText(est, getWidth() / 2 - paint.measureText(est) / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 4, paint);


            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 2, getWidth() / 100, paint);
            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 2, getWidth() / 100, paint);
            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 2, getWidth() / 100, paint);

            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawText(sud, getWidth() / 2 - paint.measureText(sud) / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 4, paint);

            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 2, getWidth() / 100, paint);
            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 2, getWidth() / 100, paint);
            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 2, getWidth() / 100, paint);

            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawText(ouest, getWidth() / 2 - paint.measureText(ouest) / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 4, paint);

            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 2, getWidth() / 100, paint);
            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 2, getWidth() / 100, paint);
            canvas.rotate(22.5f, getWidth() / 2, getHeight() / 2);
            canvas.drawCircle(getWidth() / 2, getHeight() / 2 - (getWidth() / 4.5f) - (getWidth() / 2.7f - getWidth() / 4.5f) / 2, getWidth() / 100, paint);
        }
    }
}
