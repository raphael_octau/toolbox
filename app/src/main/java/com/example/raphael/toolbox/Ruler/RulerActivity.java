package com.example.raphael.toolbox.Ruler;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PointF;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.example.raphael.toolbox.Outils;
import com.example.raphael.toolbox.R;

public class RulerActivity extends AppCompatActivity {

    private int colorPrimary;
    private int nbFingerPressed = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.ruler_title);

        colorPrimary = Outils.getColorID(this, R.attr.colorPrimary);

        setContentView(new ViewRuler(this));
    }

    /**
     * On créé le menu de changement de mode dynamiquement
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, 0, Menu.NONE, R.string.menu_info).setIcon(R.drawable.info).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case 0:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle(getResources().getString(R.string.menu_info));
                alertDialog.setMessage(getResources().getString(R.string.ruler_info));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                break;
        }
        return false;
    }

    class ViewRuler extends View {

        private Paint paint = new Paint();
        private DisplayMetrics metrics = new DisplayMetrics();

        private PointF pointF1 = null, pointF2 = null;

        public ViewRuler(Context context) {
            super(context);
            paint.setAntiAlias(true);
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            int action = (event.getAction() & MotionEvent.ACTION_MASK);
            int nbPoint = event.getPointerCount();

            if ((action == MotionEvent.ACTION_DOWN) ||
                    (action == MotionEvent.ACTION_POINTER_DOWN) ||
                    (action == MotionEvent.ACTION_MOVE)) {
                pointF1 = null;
                if (event.getPointerCount() == 2 || nbFingerPressed == 0)
                    pointF2 = null;
            }

            nbFingerPressed = event.getPointerCount();

            if ((action == MotionEvent.ACTION_UP || action == MotionEvent.ACTION_POINTER_UP) && event.getPointerCount() <2) {
                nbFingerPressed = 0;
            }

            for (int i = 0; i < nbPoint; i++)
                if ((action == MotionEvent.ACTION_DOWN) ||
                        (action == MotionEvent.ACTION_POINTER_DOWN) ||
                        (action == MotionEvent.ACTION_MOVE))
                    if (i == 0)
                        pointF1 = new PointF(event.getX(i), event.getY(i));
                    else if (i == 1)
                        pointF2 = new PointF(event.getX(i), event.getY(i));

            if (pointF1 != null && pointF2 != null) {
                if (pointF2.x < pointF1.x) {
                    float xTemp = pointF2.x;
                    pointF2.x = pointF1.x;
                    pointF1.x = xTemp;
                }
                if (pointF2.y < pointF1.y) {
                    float yTemp = pointF2.y;
                    pointF2.y = pointF1.y;
                    pointF1.y = yTemp;
                }
            }

            invalidate();

            return true;
        }

        @Override
        public void onDraw(Canvas canvas) {

            float x = 0, y = 0;

            paint.setTextSize(getWidth() / 30);
            paint.setColor(Color.BLACK);

            getWindowManager().getDefaultDisplay().getMetrics(metrics);
            float yDPI = metrics.ydpi;

            float minLine = (float) (yDPI / 25.4 * 3);
            float medLine = (float) (yDPI / 25.4 * 5);
            float maxLine = (float) (yDPI / 25.4 * 7);

            for (int i = 0; i < (getHeight() / (yDPI / 25.4)); i++) {
                if (i % 10 == 0) {
                    canvas.drawLine(0, y, maxLine, y, paint);

                    if (i != 0)
                        canvas.drawText(String.valueOf(i / 10), maxLine - paint.measureText(String.valueOf(i / 10)), y - getHeight() / 100, paint);
                } else if (i % 5 == 0)
                    canvas.drawLine(0, y, medLine, y, paint);
                else
                    canvas.drawLine(0, y, minLine, y, paint);

                y += yDPI / 25.4;
            }

            for (int i = 0; i < (getWidth() / (yDPI / 25.4)); i++) {
                if (i % 10 == 0) {
                    canvas.drawLine(x, 0, x, maxLine, paint);

                    if (i != 0)
                        canvas.drawText(String.valueOf(i / 10), x - paint.measureText(String.valueOf(i / 10)) - getHeight() / 100, maxLine, paint);
                } else if (i % 5 == 0)
                    canvas.drawLine(x, 0, x, medLine, paint);
                else
                    canvas.drawLine(x, 0, x, minLine, paint);

                x += yDPI / 25.4;
            }


            if (pointF1 != null && pointF2 != null) {
                paint.setColor(colorPrimary);
                paint.setAlpha(50);
                canvas.drawRect(pointF1.x, pointF1.y, pointF2.x, pointF2.y, paint);
                paint.setAlpha(255);

                String texte = "X=" + String.format("%.2f", pxToCm(pointF2.x - pointF1.x)) + " Y=" + String.format("%.2f", pxToCm(pointF2.y - pointF1.y)) + " D=" + String.format("%.2f", Math.sqrt(Math.pow(pxToCm(pointF2.y - pointF1.y), 2) + Math.pow(pxToCm(pointF2.x - pointF1.x), 2)));
                Rect r = new Rect();
                paint.getTextBounds(texte, 0, texte.length(), r);
                canvas.drawText(texte, pointF1.x + (pointF2.x - pointF1.x) / 2 - r.width() / 2, pointF1.y + (pointF2.y - pointF1.y) / 2 + r.height() / 2, paint);
            }
            if (pointF1 != null) {
                paint.setColor(colorPrimary);
                canvas.drawLine(pointF1.x, 0, pointF1.x, getHeight(), paint);
                canvas.drawLine(0, pointF1.y, getWidth(), pointF1.y, paint);

                Rect bounds = new Rect();
                String texte = "X=" + String.format("%.2f", pxToCm(pointF1.x)) + " Y=" + String.format("%.2f", pxToCm(pointF1.y));
                paint.getTextBounds(texte, 0, texte.length(), bounds);
                canvas.drawRect(pointF1.x, pointF1.y, pointF1.x + bounds.width() + (getWidth() / 50), pointF1.y + bounds.height() + (getWidth() / 50), paint);
                paint.setColor(Color.WHITE);
                canvas.drawText(texte, pointF1.x + (getWidth() / 50) / 2, pointF1.y + bounds.height() + (getWidth() / 50) / 2, paint);
            }
            if (pointF2 != null) {
                paint.setColor(colorPrimary);
                canvas.drawLine(pointF2.x, 0, pointF2.x, getHeight(), paint);
                canvas.drawLine(0, pointF2.y, getWidth(), pointF2.y, paint);

                Rect bounds = new Rect();
                String texte = "X=" + String.format("%.2f", pxToCm(pointF2.x)) + " Y=" + String.format("%.2f", pxToCm(pointF2.y));
                paint.getTextBounds(texte, 0, texte.length(), bounds);
                canvas.drawRect(pointF2.x - bounds.width() - (getWidth() / 50), pointF2.y - bounds.height() - (getWidth() / 50), pointF2.x, pointF2.y, paint);
                paint.setColor(Color.WHITE);
                canvas.drawText(texte, pointF2.x - bounds.width() - (getWidth() / 50) / 2, pointF2.y - (getWidth() / 50) / 2, paint);
            }
        }

    }

    private double pxToCm(final float px) {
        final DisplayMetrics dm = getResources().getDisplayMetrics();
        return px / TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 1, dm) / 10;
    }
}
