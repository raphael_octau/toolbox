package com.example.raphael.toolbox.Converter;

import android.content.res.ColorStateList;
import android.support.annotation.IdRes;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.example.raphael.toolbox.Outils;
import com.example.raphael.toolbox.R;

import java.math.BigInteger;
import java.util.Objects;

public class ConverterActivity extends AppCompatActivity {

    private TextView txtDecimal, txtBinaire, txtHexa, txtOctet;
    private EditText txtInput;
    private TextInputLayout txtInputParent;
    private RadioButton rbDecimal, rbBinaire, rbHexa, rbOctet;
    private int colorAccent;
    private ColorStateList defaultColor;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_converter);
        setTitle(R.string.converter_title);
        if (getSupportActionBar() != null) getSupportActionBar().setElevation(0);

        colorAccent = Outils.getColorID(this, R.attr.colorAccent);

        txtDecimal = (TextView) findViewById(R.id.converter_text_decimal);
        txtBinaire = (TextView) findViewById(R.id.converter_text_binaire);
        txtHexa = (TextView) findViewById(R.id.converter_text_hexa);
        txtOctet = (TextView) findViewById(R.id.converter_text_octet);
        defaultColor = txtDecimal.getTextColors();

        rbDecimal = (RadioButton) findViewById(R.id.converter_decimal);
        rbBinaire = (RadioButton) findViewById(R.id.converter_binaire);
        rbHexa = (RadioButton) findViewById(R.id.converter_hexa);
        rbOctet = (RadioButton) findViewById(R.id.converter_octets);

        RadioGroup radioGroup = (RadioGroup) findViewById(R.id.converter_radioGroup);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                if (rbDecimal.isChecked() || rbBinaire.isChecked() || rbOctet.isChecked())
                    txtInput.setInputType(InputType.TYPE_CLASS_NUMBER);
                else
                    txtInput.setInputType(InputType.TYPE_CLASS_TEXT);

                if (txtInput.getText().toString().equals("")) {
                    txtInputParent.setError(null);

                    txtDecimal.setText(null);
                    txtBinaire.setText(null);
                    txtHexa.setText(null);
                    txtOctet.setText(null);
                } else checkText();
            }
        });

        txtInputParent = (TextInputLayout) findViewById(R.id.converter_input_parent);
        txtInput = txtInputParent.getEditText();
        txtInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() > 0) checkText();
                else {
                    txtInputParent.setError(null);
                    
                    txtDecimal.setText(null);
                    txtBinaire.setText(null);
                    txtHexa.setText(null);
                    txtOctet.setText(null);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public void checkText() {
        String texte = txtInput.getText().toString();
        try {
            txtInputParent.setError(null);
            if (rbDecimal.isChecked())
                if (texte.matches("[0-9]+") && Integer.parseInt(texte) <= Integer.MAX_VALUE) {
                    txtDecimal.setText(getResources().getString(R.string.converter_decimal_res, Integer.parseInt(texte)));
                    txtBinaire.setText(getResources().getString(R.string.converter_binaire_res, Integer.toBinaryString(Integer.parseInt(texte))));
                    txtHexa.setText(getResources().getString(R.string.converter_hexa_res, Integer.toHexString(Integer.parseInt(texte)).toUpperCase()));
                    txtOctet.setText(getResources().getString(R.string.converter_octets_res, Integer.toOctalString(Integer.parseInt(texte))));

                    txtDecimal.setTextColor(colorAccent);
                    txtBinaire.setTextColor(defaultColor);
                    txtHexa.setTextColor(defaultColor);
                    txtOctet.setTextColor(defaultColor);
                } else {
                    txtInputParent.setError(getResources().getString(R.string.converter_erreur_format_dec));
                }
            else if (rbBinaire.isChecked())
                if (texte.matches("[0-1]+")) {
                    txtDecimal.setText(getResources().getString(R.string.converter_decimal_res, Integer.parseInt(texte, 2)));
                    txtBinaire.setText(getResources().getString(R.string.converter_binaire_res, texte));
                    txtHexa.setText(getResources().getString(R.string.converter_hexa_res, String.format("%X", Long.parseLong(texte, 2)).toUpperCase()));
                    txtOctet.setText(getResources().getString(R.string.converter_octets_res, Long.toOctalString(Long.parseLong(texte, 2))));

                    txtDecimal.setTextColor(defaultColor);
                    txtBinaire.setTextColor(colorAccent);
                    txtHexa.setTextColor(defaultColor);
                    txtOctet.setTextColor(defaultColor);
                } else {
                    txtInputParent.setError(getResources().getString(R.string.converter_erreur_format_bin));
                }
            else if (rbHexa.isChecked())
                if (texte.matches("[0-9a-fA-F]+")) {
                    txtDecimal.setText(getResources().getString(R.string.converter_decimal_res, Integer.parseInt(texte, 16)));
                    txtBinaire.setText(getResources().getString(R.string.converter_binaire_res, new BigInteger(texte, 16).toString(2)));
                    txtHexa.setText(getResources().getString(R.string.converter_hexa_res, texte));
                    txtOctet.setText(getResources().getString(R.string.converter_octets_res, Integer.toOctalString(Integer.parseInt(texte, 16))));

                    txtDecimal.setTextColor(defaultColor);
                    txtBinaire.setTextColor(defaultColor);
                    txtHexa.setTextColor(colorAccent);
                    txtOctet.setTextColor(defaultColor);
                } else {
                    txtInputParent.setError(getResources().getString(R.string.converter_erreur_format_hex));
                }
            else if (rbOctet.isChecked())
                if (texte.matches("[0-7]+") && Integer.parseInt(texte) < Integer.MAX_VALUE) {
                    txtDecimal.setText(getResources().getString(R.string.converter_decimal_res, Integer.parseInt(texte, 8)));
                    txtBinaire.setText(getResources().getString(R.string.converter_binaire_res, getBinaryNumber(Integer.parseInt(texte))));
                    txtHexa.setText(getResources().getString(R.string.converter_hexa_res, Integer.toHexString(Integer.parseInt(texte, 8)).toUpperCase()));
                    txtOctet.setText(getResources().getString(R.string.converter_octets_res, texte));

                    txtDecimal.setTextColor(defaultColor);
                    txtBinaire.setTextColor(defaultColor);
                    txtHexa.setTextColor(defaultColor);
                    txtOctet.setTextColor(colorAccent);
                } else {
                    txtInputParent.setError(getResources().getString(R.string.converter_erreur_format_oct));
                }
        } catch (NumberFormatException e) {
            txtInputParent.setError(getResources().getString(R.string.converter_erreur_format));
        }
        if (txtInputParent.getError() != null) {
            txtDecimal.setText(null);
            txtBinaire.setText(null);
            txtHexa.setText(null);
            txtOctet.setText(null);
        }
    }

    private static String getBinaryNumber(int octalInput) {
        int remainder;
        String binValue = "";
        do {
            remainder = (octalInput / 2) % 2;
            binValue += remainder;
            octalInput /= 2;
        } while (octalInput != 0);
        return binValue;
    }
}
