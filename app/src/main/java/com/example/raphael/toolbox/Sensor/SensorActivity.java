package com.example.raphael.toolbox.Sensor;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

import com.example.raphael.toolbox.R;

import java.util.ArrayList;
import java.util.LinkedHashMap;

public class SensorActivity extends AppCompatActivity implements SensorEventListener {

    private LinkedHashMap<Sensor, String[]> hmData;
    private ArrayList<Sensor> alSensor;
    private SensorAdapter adapter;
    private SensorManager sensorManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sensor);
        setTitle(getResources().getString(R.string.activity_sensor_title));

        hmData = new LinkedHashMap<>();
        alSensor = new ArrayList<>();

        Toast.makeText(this, getResources().getString(R.string.activity_sensor_info), Toast.LENGTH_SHORT).show();

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        int id = 0;

        alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER));
        sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
        hmData.put(alSensor.get(id++), new String[]{"ACCELEROMETER", getResources().getString(R.string.activity_sensor_aucune_valeur)});

        alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE));
        sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
        hmData.put(alSensor.get(id++), new String[]{"AMBIENT TEMPERATURE", getResources().getString(R.string.activity_sensor_aucune_valeur)});

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR));
            sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
            hmData.put(alSensor.get(id++), new String[]{"GAME ROTATION VECTOR", getResources().getString(R.string.activity_sensor_aucune_valeur)});
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR));
            sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
            hmData.put(alSensor.get(id++), new String[]{"GEOMAGNETIC ROTATION VECTOR", getResources().getString(R.string.activity_sensor_aucune_valeur)});
        }

        alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY));
        sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
        hmData.put(alSensor.get(id++), new String[]{"GRAVITY", getResources().getString(R.string.activity_sensor_aucune_valeur)});

        alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE));
        sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
        hmData.put(alSensor.get(id++), new String[]{"GYROSCOPE", getResources().getString(R.string.activity_sensor_aucune_valeur)});

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE_UNCALIBRATED));
            sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
            hmData.put(alSensor.get(id++), new String[]{"GYROSCOPE UNCALIBRATED", getResources().getString(R.string.activity_sensor_aucune_valeur)});
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE));
            sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
            hmData.put(alSensor.get(id++), new String[]{"HEART RATE", getResources().getString(R.string.activity_sensor_aucune_valeur)});
        }

        alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT));
        sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
        hmData.put(alSensor.get(id++), new String[]{"LIGHT", getResources().getString(R.string.activity_sensor_aucune_valeur)});

        alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION));
        sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
        hmData.put(alSensor.get(id++), new String[]{"LINEAR ACCELERATION", getResources().getString(R.string.activity_sensor_aucune_valeur)});

        alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD));
        sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
        hmData.put(alSensor.get(id++), new String[]{"MAGNETIC FIELD", getResources().getString(R.string.activity_sensor_aucune_valeur)});

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED));
            sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
            hmData.put(alSensor.get(id++), new String[]{"MAGNETIC FIELD UNCALIBRATED", getResources().getString(R.string.activity_sensor_aucune_valeur)});
        }

        alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE));
        sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
        hmData.put(alSensor.get(id++), new String[]{"PRESSURE", getResources().getString(R.string.activity_sensor_aucune_valeur)});

        alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY));
        sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
        hmData.put(alSensor.get(id++), new String[]{"PROXIMITY", getResources().getString(R.string.activity_sensor_aucune_valeur)});

        alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY));
        sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
        hmData.put(alSensor.get(id++), new String[]{"RELATIVE HUMIDITY", getResources().getString(R.string.activity_sensor_aucune_valeur)});

        alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR));
        sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
        hmData.put(alSensor.get(id++), new String[]{"ROTATION VECTOR", getResources().getString(R.string.activity_sensor_aucune_valeur)});

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_SIGNIFICANT_MOTION));
            sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
            hmData.put(alSensor.get(id++), new String[]{"SIGNIFICANT MOTION", getResources().getString(R.string.activity_sensor_aucun_declencheur)});
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_STEP_COUNTER));
            sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
            hmData.put(alSensor.get(id++), new String[]{"STEP COUNTER", getResources().getString(R.string.activity_sensor_aucun_declencheur)});
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            alSensor.add(sensorManager.getDefaultSensor(Sensor.TYPE_STEP_DETECTOR));
            sensorManager.registerListener(this, alSensor.get(alSensor.size() - 1), SensorManager.SENSOR_DELAY_NORMAL);
            hmData.put(alSensor.get(id), new String[]{"STEP DETECTOR", getResources().getString(R.string.activity_sensor_aucun_declencheur)});
        }

        adapter = new SensorAdapter(this, hmData);
        ListView lv = (ListView) findViewById(R.id.sensor_listview);
        lv.setAdapter(adapter);
    }

    /**
     * On créé le menu de changement de mode dynamiquement
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, 0, Menu.NONE, R.string.menu_refresh).setIcon(R.drawable.refresh).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case 0:
                adapter.notifyDataSetChanged();
                break;
        }
        return false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        for (Sensor sensor : alSensor)
            sensorManager.registerListener(this, sensor, SensorManager.SENSOR_DELAY_NORMAL);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER))[1] = "X = " + event.values[0] + " m/s2\nY = " + event.values[1] + " m/s2\nZ = " + event.values[2] + " m/s2";
        } else if (event.sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE))[1] = "Temp = " + event.values[0] + " °C";
        } else if (event.sensor.getType() == Sensor.TYPE_GAME_ROTATION_VECTOR && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR))[1] = "X = " + event.values[0] + "\nY = " + event.values[1] + "\nZ = " + event.values[2];
        } else if (event.sensor.getType() == Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_GEOMAGNETIC_ROTATION_VECTOR))[1] = "X = " + event.values[0] + "\nY = " + event.values[1] + "\nZ = " + event.values[2];
        } else if (event.sensor.getType() == Sensor.TYPE_GRAVITY) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY))[1] = "X = " + event.values[0] + " m/s2\nY = " + event.values[1] + " m/s2\nZ = " + event.values[2] + " m/s2";
        } else if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE))[1] = "X = " + event.values[0] + "\nY = " + event.values[1] + "\nZ = " + event.values[2];
        } else if (event.sensor.getType() == Sensor.TYPE_GYROSCOPE_UNCALIBRATED && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE_UNCALIBRATED))[1] = "Xuncalib = " + event.values[0] + "\nYuncalib = " + event.values[1] + "\nZuncalib = " + event.values[2] + "\nX = " + event.values[3] + "\nY = " + event.values[4] + "\nZ = " + event.values[5];
        } else if (event.sensor.getType() == Sensor.TYPE_HEART_RATE && Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT_WATCH) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_HEART_RATE))[1] = event.values[0] + " bpm";
        } else if (event.sensor.getType() == Sensor.TYPE_LIGHT) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_LIGHT))[1] = event.values[0] + " lux";
        } else if (event.sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION))[1] = "X = " + event.values[0] + " m/s2\nY = " + event.values[1] + " m/s2\nZ = " + event.values[2] + " m/s2";
        } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD))[1] = "X = " + event.values[0] + " µT\nY = " + event.values[1] + " µT\nZ = " + event.values[2] + " µT";
        } else if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD_UNCALIBRATED))[1] = "Xuncalib = " + event.values[0] + "\nYuncalib = " + event.values[1] + "\nZuncalib = " + event.values[2] + "\nX = " + event.values[3] + "\nY = " + event.values[4] + "\nZ = " + event.values[5];
        } else if (event.sensor.getType() == Sensor.TYPE_PRESSURE) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE))[1] = event.values[0] + " hPa";
        } else if (event.sensor.getType() == Sensor.TYPE_PROXIMITY) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY))[1] = event.values[0] + " cm";
        } else if (event.sensor.getType() == Sensor.TYPE_RELATIVE_HUMIDITY) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY))[1] = event.values[0] + " %";
        } else if (event.sensor.getType() == Sensor.TYPE_ROTATION_VECTOR) {
            hmData.get(sensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR))[1] = "X = " + event.values[0] + "\nY = " + event.values[1] + "\nZ = " + event.values[2];
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
