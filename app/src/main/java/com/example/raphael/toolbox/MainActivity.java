package com.example.raphael.toolbox;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.raphael.toolbox.BubbleLevel.BubbleLevelActivity;
import com.example.raphael.toolbox.Compass.CompassActivity;
import com.example.raphael.toolbox.Converter.ConverterActivity;
import com.example.raphael.toolbox.FlashLight.FlashLightActivity;
import com.example.raphael.toolbox.Multitouch.MultitouchActivity;
import com.example.raphael.toolbox.Random.RandomActivity;
import com.example.raphael.toolbox.Ruler.RulerActivity;
import com.example.raphael.toolbox.Sensor.SensorActivity;
import com.example.raphael.toolbox.Seismograph.SeismographActivity;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Pour l'ellipsize si le texte est trop long
        findViewById(R.id.layout_compass_txt).setSelected(true);
        findViewById(R.id.layout_bubblelevel_txt).setSelected(true);
        findViewById(R.id.layout_flashlight_txt).setSelected(true);
        findViewById(R.id.layout_multitouch_txt).setSelected(true);
        findViewById(R.id.layout_converter_txt).setSelected(true);
        findViewById(R.id.layout_random_txt).setSelected(true);
        findViewById(R.id.layout_ruler_txt).setSelected(true);
        findViewById(R.id.layout_sismo_txt).setSelected(true);

        //On initialise la liste d'associations d'appli avec ses capteurs
        HashMap<Integer, ArrayList<String>> hmSensorAssociation = new HashMap<>();
        hmSensorAssociation.put(0, new ArrayList<String>() {{
            add(PackageManager.FEATURE_SENSOR_ACCELEROMETER);
            add(PackageManager.FEATURE_SENSOR_COMPASS);
        }});
        hmSensorAssociation.put(1, new ArrayList<String>() {{
            add(PackageManager.FEATURE_SENSOR_ACCELEROMETER);
        }});
        hmSensorAssociation.put(2, new ArrayList<String>() {{
            add(PackageManager.FEATURE_CAMERA);
            add(PackageManager.FEATURE_CAMERA_FLASH);
        }});
        hmSensorAssociation.put(3, new ArrayList<String>() {{
            add(PackageManager.FEATURE_SENSOR_ACCELEROMETER);
        }});
        hmSensorAssociation.put(4, new ArrayList<String>());
        hmSensorAssociation.put(5, new ArrayList<String>());
        hmSensorAssociation.put(6, new ArrayList<String>());
        hmSensorAssociation.put(7, new ArrayList<String>() {{
            add(PackageManager.FEATURE_SENSOR_ACCELEROMETER);
        }});

        //On parcours tous les elements du menu
        //et on les desactive si le capteur correspondant n'est pas disponnible
        //sinon on leur associe une appli
        LinearLayout ll0 = (LinearLayout) findViewById(R.id.layout_main);
        int cpt = 0;
        for (int i = 0; i < ll0.getChildCount(); i++) {
            if (ll0.getChildAt(i) instanceof LinearLayout) {
                LinearLayout ll1 = (LinearLayout) ll0.getChildAt(i);
                for (int j = 0; j < ll1.getChildCount(); j++) {
                    if (ll1.getChildAt(j) instanceof LinearLayout) {
                        LinearLayout ll2 = (LinearLayout) ll1.getChildAt(j);
                        boolean bFullAvailable = true;
                        String capteursIndispo = "";
                        for (String s : hmSensorAssociation.get(cpt++)) {
                            if (!getPackageManager().hasSystemFeature(s)) {
                                bFullAvailable = false;
                                capteursIndispo += "- " + s.split("\\.")[s.split("\\.").length - 1] + "\n";
                            }
                        }
                        if (!bFullAvailable) { //Si tous les capteurs ne sont pas disponibles
                            ll2.setBackgroundColor(Color.rgb(255, 200, 200));
                            ImageView img = (ImageView) ll2.getChildAt(0);
                            img.setColorFilter(Color.rgb(255, 100, 100));
                            TextView txt = (TextView) ll2.getChildAt(1);
                            txt.setText(getResources().getString(R.string.capteur_indispo));
                            final String finalCapteursIndispo = capteursIndispo;
                            ll2.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Toast.makeText(MainActivity.this, getResources().getString(R.string.capteur_indispo_liste, "\n" + finalCapteursIndispo.toUpperCase()), Toast.LENGTH_LONG).show();
                                }
                            });
                        } else {
                            ll2.setOnClickListener(this);
                        }
                    }
                }
            }
        }


        resizeToSquare();
    }

    /**
     * Methode qui permet de redimensionner les elements du menu pour les rendre carré
     */
    private void resizeToSquare() {
        //On recupère la taille de l'ecran
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        LinearLayout ll = (LinearLayout) findViewById(R.id.layout_main);
        int div = (ll.getTag().equals("layout_default")) ? 2 : 3;
        final int childcount = ll.getChildCount();
        for (int i = 0; i < childcount; i++) {
            if (ll.getChildAt(i) instanceof LinearLayout) {
                LinearLayout llChild = (LinearLayout) ll.getChildAt(i);
                llChild.setLayoutParams(new LinearLayout.LayoutParams(llChild.getLayoutParams().width, displayMetrics.widthPixels / div));
            }
        }
    }

    @Override
    public void onClick(View v) {
        if (v == findViewById(R.id.layout_bubblelevel)) {
            Intent i = new Intent(this, BubbleLevelActivity.class);
            startActivity(i);
        } else if (v == findViewById(R.id.layout_multitouch)) {
            Intent i = new Intent(this, MultitouchActivity.class);
            startActivity(i);
        } else if (v == findViewById(R.id.layout_random)) {
            Intent i = new Intent(this, RandomActivity.class);
            startActivity(i);
        } else if (v == findViewById(R.id.layout_flashlight)) {
            Intent i = new Intent(this, FlashLightActivity.class);
            startActivity(i);
        } else if (v == findViewById(R.id.layout_compass)) {
            Intent i = new Intent(this, CompassActivity.class);
            startActivity(i);
        } else if (v == findViewById(R.id.layout_converter)) {
            Intent i = new Intent(this, ConverterActivity.class);
            startActivity(i);
        } else if (v == findViewById(R.id.layout_ruler)) {
            Intent i = new Intent(this, RulerActivity.class);
            startActivity(i);
        } else if (v == findViewById(R.id.layout_sismo)) {
            Intent i = new Intent(this, SeismographActivity.class);
            startActivity(i);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_copyright:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle(getResources().getString(R.string.menu_about_title));
                alertDialog.setMessage(getResources().getString(R.string.menu_about_text));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                return true;
            case R.id.menu_sensor:
                Intent i = new Intent(this, SensorActivity.class);
                startActivity(i);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }


}
