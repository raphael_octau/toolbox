package com.example.raphael.toolbox.FlashLight;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.hardware.Camera;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;

import com.example.raphael.toolbox.R;

@SuppressLint("InlinedApi")
@SuppressWarnings("deprecation")
public class FlashLightActivity extends AppCompatActivity {

    private FlashLightView view;

    private Camera camera;

    private CameraManager cameraManager;
    private String idCamera;

    private boolean lampeAllumee;
    private boolean ecranFlashAllume;
    private boolean versionOK;

    private int luminosite;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        view = new FlashLightView(this);
        setContentView(view);
        setTitle(R.string.flashlight_title);

        try {
            luminosite = Settings.System.getInt(getContentResolver(), Settings.System.SCREEN_BRIGHTNESS);
        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }

        lampeAllumee = false;
        ecranFlashAllume = false;

        // Vérifie si la version peut supporter Camera2
        versionOK = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP;

        if (versionOK) {
            cameraManager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            try {
                idCamera = cameraManager.getCameraIdList()[0];
            } catch (CameraAccessException e) {
                e.printStackTrace();
            }
        } else {
            camera = Camera.open();
        }

    }

    public void actionLight() {
        Camera.Parameters param;
        if (!lampeAllumee) {
            if (versionOK) {
                try {
                    cameraManager.setTorchMode(idCamera, true);
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            } else {
                param = camera.getParameters();
                param.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                camera.setParameters(param);
                camera.startPreview();
            }

            lampeAllumee = true;
            view.invalidate();

        } else {
            if (versionOK) {
                try {
                    cameraManager.setTorchMode(idCamera, false);
                } catch (CameraAccessException e) {
                    e.printStackTrace();
                }
            } else {
                param = camera.getParameters();
                param.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                camera.setParameters(param);
                camera.stopPreview();
            }

            lampeAllumee = false;
            view.invalidate();
        }
    }

    public void actionScreen() {
        if (!ecranFlashAllume) {
            WindowManager.LayoutParams layoutParams = getWindow().getAttributes();

            layoutParams.screenBrightness = 1f;
            getWindow().setAttributes(layoutParams);

            ecranFlashAllume = true;
            view.invalidate();
        } else {
            WindowManager.LayoutParams layoutParams = getWindow().getAttributes();

            layoutParams.screenBrightness = luminosite;
            getWindow().setAttributes(layoutParams);

            ecranFlashAllume = false;
            view.invalidate();
        }
    }

    private class FlashLightView extends View implements View.OnTouchListener {

        Paint paintButton = new Paint();

        int xBouton, yBouton;
        int tailleBouton;

        Drawable btnTel;

        public FlashLightView(Context context) {
            super(context);
            setFocusable(true);

            paintButton.setAntiAlias(true);
            paintButton.setStyle(Paint.Style.FILL_AND_STROKE);
            paintButton.setStrokeWidth(5);

            this.setOnTouchListener(this);
        }

        public void onDraw(Canvas canvas) {

            tailleBouton = getWidth() / 4;

            xBouton = canvas.getWidth() / 2;
            yBouton = canvas.getHeight() / 2;

            if (!ecranFlashAllume) {
                canvas.drawColor(Color.rgb(50, 50, 50));
                paintButton.setColor(Color.rgb(40, 40, 40));
            } else {
                canvas.drawColor(Color.WHITE);
                paintButton.setColor(Color.rgb(230, 230, 230));
            }

            canvas.drawCircle(xBouton, yBouton, getWidth() / 3.5f, paintButton);

            if (!ecranFlashAllume)
                if (!lampeAllumee) paintButton.setColor(Color.rgb(255, 193, 7));
                else paintButton.setColor(Color.rgb(255, 87, 34));
            else if (!lampeAllumee) paintButton.setColor(Color.rgb(255, 236, 179));
            else paintButton.setColor(Color.rgb(255, 204, 188));

            canvas.drawCircle(xBouton, yBouton, tailleBouton, paintButton);

            Drawable btnFlash = ContextCompat.getDrawable(getContext(), R.drawable.power);
            btnFlash.setBounds(xBouton - (tailleBouton / 2), yBouton - (tailleBouton / 2), xBouton + (tailleBouton / 2), yBouton + (tailleBouton / 2));
            if (!lampeAllumee)
                btnFlash.setColorFilter(Color.rgb(255, 224, 130), PorterDuff.Mode.MULTIPLY);
            else
                btnFlash.setColorFilter(Color.rgb(255, 171, 145), PorterDuff.Mode.MULTIPLY);
            btnFlash.draw(canvas);

            if (!ecranFlashAllume) {
                btnTel = ContextCompat.getDrawable(getContext(), R.drawable.screen_on);
            } else {
                btnTel = ContextCompat.getDrawable(getContext(), R.drawable.screen_off);
            }

            btnTel.setBounds(getWidth() - (getWidth() / 5), getHeight() - (getWidth() / 5), getWidth() - (getWidth() / 20), getHeight() - (getWidth() / 20));

            btnTel.draw(canvas);
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (checkZoneCircle((int) event.getX(), (int) event.getY())) actionLight();
            else if (checkZoneTel((int) event.getX(), (int) event.getY())) actionScreen();

            return false;
        }

        private boolean checkZoneCircle(int x, int y) {
            return x > xBouton - tailleBouton && x < xBouton + tailleBouton &&
                    y > yBouton - tailleBouton && y < yBouton + tailleBouton;
        }

        private boolean checkZoneTel(int x, int y) {
            return x > getWidth() - (getWidth() / 5) && x < getWidth() && y > getHeight() - (getWidth() / 5) && y < getHeight();
        }
    }
}
