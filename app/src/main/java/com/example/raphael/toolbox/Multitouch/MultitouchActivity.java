package com.example.raphael.toolbox.Multitouch;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;

import com.example.raphael.toolbox.Outils;
import com.example.raphael.toolbox.R;

import java.util.ArrayList;

public class MultitouchActivity extends AppCompatActivity {

    private int colorAccent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.multitouch_title);

        colorAccent = Outils.getColorID(this, R.attr.colorAccent);

        setContentView(new MultiTouchView(this));
    }

    /**
     * On créé le menu de changement de mode dynamiquement
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, 0, Menu.NONE, R.string.menu_info).setIcon(R.drawable.info).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case 0:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle(getResources().getString(R.string.menu_info));
                alertDialog.setMessage(getResources().getString(R.string.multitouch_info));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                break;
        }
        return false;
    }

    class MultiTouchView extends View {

        private Paint paint;
        private ArrayList<Doigt> alDoigts;
        private Rect zoneTexte;

        public MultiTouchView(Context context) {
            super(context);
            zoneTexte = new Rect();
            paint = new Paint(Paint.ANTI_ALIAS_FLAG);
            alDoigts = new ArrayList<>();

        }

        @Override
        protected void onDraw(Canvas canvas) {
            paint.setColor(Color.LTGRAY);
            paint.setStyle(Paint.Style.FILL);
            paint.setTextSize(getWidth() / 2);
            String texte = String.valueOf(alDoigts.size());
            //On centre le texte
            canvas.getClipBounds(zoneTexte);
            int cHeight = zoneTexte.height();
            int cWidth = zoneTexte.width();
            paint.setTextAlign(Paint.Align.LEFT);
            paint.getTextBounds(texte, 0, texte.length(), zoneTexte);
            float x = cWidth / 2f - zoneTexte.width() / 2f - zoneTexte.left;
            float y = cHeight / 2f + zoneTexte.height() / 2f - zoneTexte.bottom;
            //On l'ecrit
            canvas.drawText(texte, x, y, paint);

            paint.setColor(colorAccent);
            paint.setStyle(Paint.Style.STROKE);
            paint.setStrokeWidth(5);
            for (Doigt doigt : alDoigts) {
                canvas.drawCircle(doigt.getX(), doigt.getY(), getWidth() / 8, paint);
            }
        }

        @Override
        public boolean onTouchEvent(MotionEvent event) {
            int action = (event.getAction() & MotionEvent.ACTION_MASK);
            int nbPoint = event.getPointerCount();

            alDoigts.clear();

            for (int i = 0; i < nbPoint; i++)
                if ((action == MotionEvent.ACTION_DOWN) ||
                        (action == MotionEvent.ACTION_POINTER_DOWN) ||
                        (action == MotionEvent.ACTION_MOVE))
                    alDoigts.add(new Doigt(event.getX(i), event.getY(i)));

            invalidate();
            return true;
        }

    }

    private class Doigt {
        private float x, y;

        Doigt(float x, float y) {
            this.x = x;
            this.y = y;
        }

        float getX() {
            return x;
        }

        float getY() {
            return y;
        }
    }

}
