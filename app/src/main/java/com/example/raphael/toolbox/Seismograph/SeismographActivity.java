package com.example.raphael.toolbox.Seismograph;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.Shader;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.raphael.toolbox.R;

import java.util.ArrayList;

public class SeismographActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;

    private ViewSismo viewSismo;

    private ArrayList<Float> alPoints;

    private float mAccel; // acceleration sans gravité
    private float mAccelCurrent; // acceleration actuelle avec gravité
    private float mAccelLast; // acceleration precedente avec gravité
    private long lastUpdate = 0; // pour mettre un temps de latence

    private final int MAX_SIZE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(R.string.sismo_title);

        viewSismo = new ViewSismo(this);

        setContentView(viewSismo);

        alPoints = new ArrayList<>();


        mAccel = 0.00f;
        mAccelCurrent = SensorManager.GRAVITY_EARTH;
        mAccelLast = SensorManager.GRAVITY_EARTH;


        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                SensorManager.SENSOR_DELAY_FASTEST);
    }

    /**
     * On créé le menu de changement de mode dynamiquement
     */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        menu.clear();
        menu.add(0, 0, Menu.NONE, R.string.menu_info).setIcon(R.drawable.info).setShowAsAction(MenuItem.SHOW_AS_ACTION_IF_ROOM);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);

        switch (item.getItemId()) {
            case 0:
                AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                alertDialog.setTitle(getResources().getString(R.string.menu_info));
                alertDialog.setMessage(getResources().getString(R.string.sismo_info));
                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.ok),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        });
                alertDialog.show();
                break;
        }
        return false;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            long curTime = System.currentTimeMillis();
            // temps de latence de 20ms ou 0ms si alPoints est pas encore repmli completement
            if ((curTime - lastUpdate) > ((alPoints.size() < MAX_SIZE) ? 0 : 100)) {
                lastUpdate = curTime;

                float x = event.values[0];
                float y = event.values[1];
                float z = event.values[2];
                mAccelLast = mAccelCurrent;
                mAccelCurrent = (float) Math.sqrt((double) (x * x + y * y + z * z));
                float delta = mAccelCurrent - mAccelLast;
                mAccel = mAccel * 0.9f + delta; // on applique un filtre

                //On ajoute le point et son opposé
                alPoints.add(mAccel);
                alPoints.add(-mAccel);

                //on enleve les deux plus anciens points
                if (alPoints.size() > MAX_SIZE) {
                    alPoints.remove(0);
                    alPoints.remove(0);
                }
                viewSismo.invalidate();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
    }

    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    class ViewSismo extends View {

        private Paint paint = new Paint();
        private Path path = new Path();
        private Rect zoneTexte = new Rect();
        private int maxHeight, normalHeight, minHeight;
        private ArrayList<Integer> rainbow = new ArrayList<>();

        public ViewSismo(Context context) {
            super(context);
            paint.setAntiAlias(true);
        }

        @Override
        public void onDraw(Canvas canvas) {
            maxHeight = getHeight() / 2 - getHeight() / 3;
            normalHeight = getHeight() / 2;
            minHeight = getHeight() / 2 + getHeight() / 3;

            if (alPoints.size() < MAX_SIZE) return; //Si alPoints n'est pas completement remplie

            //On dessine les trois lignes
            paint.setTextSize(getWidth() / 30);
            String texteMax = getResources().getString(R.string.sismo_max);
            paint.getTextBounds(texteMax, 0, texteMax.length(), zoneTexte);
            canvas.drawLine(0, maxHeight, getWidth() - zoneTexte.width() - getWidth() / 50, maxHeight, paint);
            canvas.drawText(texteMax, getWidth() - zoneTexte.width() - (getWidth() / 50) / 2, maxHeight + zoneTexte.height() / 2, paint);

            canvas.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2, paint);

            canvas.drawLine(0, minHeight, getWidth() - zoneTexte.width() - getWidth() / 50, minHeight, paint);
            canvas.drawText(texteMax, getWidth() - zoneTexte.width() - (getWidth() / 50) / 2, minHeight + zoneTexte.height() / 2, paint);

            //On dessine le graph
            paint.setShader(null);
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(4);
            paint.setStrokeJoin(Paint.Join.ROUND);
            paint.setStrokeCap(Paint.Cap.ROUND);
            paint.setStyle(Paint.Style.STROKE);
            rainbow.clear();
            path.reset();
            float step = getWidth() / MAX_SIZE, x = 0;
            int i = 0;
            while (i < MAX_SIZE - 1) {
                path.moveTo(x, normalizeY(alPoints.get(i)));
                x += step;
                path.lineTo(x, normalizeY(alPoints.get(i + 1)));
                rainbow.add(Color.rgb((int) ((Math.abs(normalizeY(alPoints.get(i + 1) - alPoints.get(i)) - normalHeight)) * 255 / (getHeight() / 4) / 1.5), 0, 0));
                i++;
            }
            path.close();
            //On applique les couleurs d'intensité
            int[] tabColors = new int[rainbow.size()];
            for (int coul = 0; coul < rainbow.size(); coul++) tabColors[coul] = rainbow.get(coul);
            Shader shader = new LinearGradient(0, 0, 0, x, tabColors, null, Shader.TileMode.REPEAT);
            Matrix matrix = new Matrix();
            matrix.setRotate(-90);
            shader.setLocalMatrix(matrix);
            paint.setShader(shader);
            canvas.drawPath(path, paint);

            paint.setShader(null);
            paint.setColor(Color.BLACK);
            paint.setStrokeWidth(0);
            paint.setStyle(Paint.Style.FILL);

            //On dessine la pointe
            path.reset();
            path.moveTo(x, normalizeY(alPoints.get(alPoints.size() - 1)));
            path.lineTo(getWidth(), normalizeY(alPoints.get(alPoints.size() - 1)) - getHeight() / 100);
            path.lineTo(getWidth(), normalizeY(alPoints.get(alPoints.size() - 1)) + getHeight() / 100);
            path.lineTo(x, normalizeY(alPoints.get(alPoints.size() - 1)));
            path.lineTo(x, normalizeY(alPoints.get(alPoints.size() - 1)));
            path.close();
            canvas.drawPath(path, paint);
        }

        /**
         * Permet de normaliser y en fontion du graph
         *
         * @param y l'initial
         * @return le normalisé
         */
        private float normalizeY(float y) {
            float res = y * getHeight() / 4 + normalHeight;
            if (res < maxHeight) return maxHeight;
            else if (res > minHeight) return minHeight;
            return res;
        }

    }
}
