[![Licence](https://img.shields.io/badge/license-CC%20BY--NC--ND%204.0-blue.svg)](LICENSE)

## A propos du projet

Ce projet est une application regroupant plusieurs outils pratiques à savoir :
- un niveau à bulles
- un sismographe
- une lampe torche
- une boussole
- un convertisseur de bases
- une règle graduée
- un capteur « multitouch »
- un générateur de nombre aléatoire.

Ces outils utilisent pour la plupart les différents capteurs présents par défaut dans un appareil Android.

Pour installer le projet :

1. Ouvrez Android Studio
2. Compilez les sources
3. Testez sur un émulateur ou un appareil branché par câble USB

Pour voir mes autres projets, rendez vous sur mon [portfolio](https://raphael-octau.fr/en).

## TODOs

- [ ] Ajouter d'autres projets

## Licence

&copy; 2019 Raphaël OCTAU

Ce travail est sous licence [Creative Commons (Attribution - Pas d'Utilisation Commerciale - Pas de Modification) 4.0 International](LICENSE).
